<div id="updateRoleModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' => 'form',
						'url' => '#',
						'method' => 'post',
						'class' => 'form-horizontal',
						'id'=> 'update_role'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							@include('form.text', [
								'field' => 'name',
								'label' => 'Nama',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'update_name'
								]
							])

							@include('form.textarea', [
								'field' => 'description',
								'label' => 'Description',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'update_description',
									'rows' => 5,
									'style' => 'resize: none;'
								]
							])

							@include('form.select', [
								'field' => 'permission',
								'label' => 'Permission',
								'options' => [
									'' => '-- Pilih Permission --',
								]+$permissions,
								'class' => 'select-search',
								'attributes' => [
									'id' => 'update_select_permission'
								]
							])
						</div>
						{!! Form::hidden('role_id',null, array('id' => 'role_id')) !!}
						{!! Form::hidden('mappings', '[]', array('id' => 'update_mappings')) !!}
						{!! Form::close() !!}

						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h6 class="panel-title text-semibold">Role Permission &nbsp; <span class="label label-info heading-text">Mapping Role with Permission Role.</span></h6>
								</div>

								<div class="panel-body">
									<div class="row">
										<div class="table-responsive">
											<table class="table table-basic table-condensed" id="permissionTable">
												<thead w>
													<tr>
														<th>No</th>
														<th>Permission</th>
														<th>Action</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									
								</div>
							</div>
						</div>

						
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="submit_button">Submit form</button>
				</div>
			
		</div>
	</div>
</div>