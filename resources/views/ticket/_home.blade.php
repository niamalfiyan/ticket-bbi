{{-- <br>
<span class="label label bg-teal label-block">
    <h1 class="no-margin text-blod text-center"><mark>SELAMAT DATANG</mark></h1>
    <hr class="style14">
    <p class="text-center">
        <code>
        Halo!<br>This is a new website for ticket system. Brand new year brand new website, right? <br>But it's okay we keep it like the old ones to make sure it's easy for you to use.<br>We improved some of the features, like a stastical number to show your overall ticket's status above<br>
        and there are much more to discover. <br><var>Enjoy</var>.
        </code>
    </p>
</span>
<br> --}}

<!-- Quick stats boxes -->
<div class="row">
	<div class="col-lg-4">

		<!-- Members online -->
		<div class="panel bg-pink-300">
			<div class="panel-body">
				<div class="col-md-5">
					<div class="heading-elements">
						{{-- <span class="label label-primary label-rounded">TOTAL SUPPORT OPEN : {{ $ticket_support_open_percent }}%</span>
						<span class="heading-text badge bg-indigo-800">+53,6%</span> --}}
					</div>
	
					<h3 class="no-margin">{{ $total_support }}</h3>
					Support Ticket
					<div class="text-muted text-size-small">{{ $ticket_support_today}} Ticket Hari Ini</div>
				</div>
				
				<div class="col-md-7">
					
					<label>Closed</label>
					<div class="progress progress-lg">
						<div class="progress-bar progress-bar-striped active bg-success-400" style="width: {{ $ticket_support_closed_percent }}%">
							<span>{{ $ticket_support_closed_percent }}% Complete</span>
						</div>
					</div>
					{{-- <br>
					<label>Open</label>
					<div class="progress progress-lg">
						<div class="progress-bar progress-bar-striped active bg-danger-400" style="width: {{ $ticket_support_open_percent }}%">
							<span>{{ $ticket_support_open_percent }}% Complete</span>
						</div>
					</div> --}}

				</div>
				
				
			</div>

			<div class="container-fluid">
				<div id="members-online"></div>
			</div>
		</div>
		<!-- /members online -->

	</div>

	
	<div class="col-lg-4">

		<!-- Today's revenue -->
		{{-- <div class="panel bg-slate">
			<div class="panel-body">
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="reload"></a></li>
					</ul>
				</div>

				<h3 class="no-margin">$18,390</h3>
				Today's revenue
				<div class="text-muted text-size-small">$37,578 avg</div>
			</div>

			<div id="today-revenue"></div>
		</div> --}}
		<!-- /today's revenue -->

	</div>

	<div class="col-lg-4">

		<!-- Current server load -->
		<div class="panel bg-purple-400">
			<div class="panel-body">
				<div class="col-md-5">
					<div class="heading-elements">
						{{-- <span class="label label-primary label-rounded">TOTAL SUPPORT OPEN : {{ $ticket_support_open_percent }}%</span>
						<span class="heading-text badge bg-indigo-800">+53,6%</span> --}}
					</div>
	
					<h3 class="no-margin">{{ $total_request }}</h3>
					Request Ticket
					<div class="text-muted text-size-small">{{ $ticket_request_today}} Ticket Hari Ini</div>
				</div>
				
				<div class="col-md-7">
					
					<label>Closed</label>
					<div class="progress progress-lg">
						<div class="progress-bar progress-bar-striped active bg-success-400" style="width: {{ $ticket_request_closed_percent }}%">
							<span>{{ $ticket_request_closed_percent }}% Complete</span>
						</div>
					</div>
					{{-- <br>
					<label>Open</label>
					<div class="progress progress-lg">
						<div class="progress-bar progress-bar-striped active bg-danger-400" style="width: {{ $ticket_support_open_percent }}%">
							<span>{{ $ticket_support_open_percent }}% Complete</span>
						</div>
					</div> --}}

				</div>
			</div>

			<div id="server-load"></div>
		</div>
		<!-- /current server load -->

	</div>

</div>
<!-- /quick stats boxes -->