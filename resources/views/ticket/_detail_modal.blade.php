<div id="detailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Ticket</h5>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
		
			<div class="modal-body">
				<div class="tabbable tab-content-bordered" id="tab" class="hidden">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#detail-ticket" data-toggle="tab" aria-expanded="false" onclick="changeDetailUser('progress')">Progress</a></li>
						<li><a href="#comment-ticket" data-toggle="tab" aria-expanded="false" onclick="changeDetailUser('comment')">Comment</a></li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="detail-ticket">
							{!! Form::hidden('ticket_id',null, array('id' => 'ticket_id')) !!}
							<br>
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="progressTable" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>ID</th>
											<th>Created At</th>
											<th>Status</th>
											<th>User</th>
											<th>Remark</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
						<div class="tab-pane" id="comment-ticket">
							<br>
							<div class="table-responsive">
								<table class="table datatable-basic table-striped table-hover table-responsive" id="commentTable" style="width:100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Created At</th>
											<th>User</th>
											<th>Comment</th>
											<th>Attachment</th>
										</tr>
									</thead>
								</table>
							</div>
							<br/>
							<!-- Solid alert -->
							<div class="alert alert-info alert-styled-left alert-bordered">
								<span class="text-semibold">Pemberitahuan!</span> Anda bisa memberi atau membalas komentar dibawah sini.
							</div>
							<!-- /solid alert -->
                            {!!
                                Form::open([
                                    'role'   => 'form',
                                    'url'    => route('ticket.addComment'),
                                    'method' => 'post',
                                    'class'  => 'form-horizontal',
                                    'id'     => 'user_insert_comment'
                                ])
                            !!}
                            <div class="modal-body">
                                @include('form.textarea', [
                                    'field'      => 'comment',
                                    'label'      => 'Add Comment',
                                    'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                                    'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                                    'attributes' => [
                                        'id'    => 'comment',
                                        'rows'  => 5,
                                        'style' => 'resize: none;'
                                    ]
								])

								@include('form.file', [
									'field'      => 'c_upload',
									'label'      => 'Attachment',
									'mandatory'  => '*Max 5MB',
									'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
									'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
									'attributes' => [
										'id'       => 'c_upload',
										'rows'     => 5,
										'style'    => 'resize: none;',
										// 'required' => ''
									]
								])
								
								{!! Form::hidden('_ticket_id',null, array('id' => '_ticket_id')) !!}
								{!! Form::hidden('_user_id',$data_user->nik, array('id' => '_user_id')) !!}
                                </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        	{!! Form::close() !!}
					
							
					</div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::hidden('active_detail_user',null, array('id' => 'active_detail_user')) !!}
			</div>
		</div>
	</div>
</div>
