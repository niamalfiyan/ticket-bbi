@extends('layouts.app',['active' => 'admin-support'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Ticket Support</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Ticket Support</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
	</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="adminSupportTable">
                <thead>
                    <tr>
						<th>ID</th>
						<th>Kode Ticket</th>
						<th></th>
                        <th>Status</th>
                        <th>Nama</th>
                        <th>Dept</th>
                        <th>Judul</th>
                        <th>Created_at</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('admin-support._support_modal')
@endsection

@section('page-js')
<script>
$(document).ready( function () {
	$('#adminSupportTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		deferRender:true,
		ajax: {
			type: 'GET',
			url: '/admin-support/data',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'code', name: 'code',searchable:true,orderable:false},
			{data: 'notifikasi', name: 'notifikasi',searchable:true,orderable:true,  width: '10%'},
			{data: 'last_status', name: 'last_status',searchable:true,orderable:true},
			{data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'department', name: 'department',searchable:true,orderable:true},
            {data: 'issue_category', name: 'issue_category',searchable:true,orderable:true},
			{data: 'title', name: 'title',searchable:true,orderable:true},
			{data: 'created_at', name: 'created_at',searchable:true,orderable:true},
			{data: 'factory', name: 'factory',searchable:true,orderable:true},
		]
	});

	var dtable = $('#adminSupportTable').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#update_progres_ticket').submit(function (event){
		event.preventDefault();
		var status = $('#select_status').val();
		var remark = $('#remark').val();

		if(!status){
			$("#alert_warning").trigger("click", 'Status wajib dipilih');
			return false
		}
		else if(!remark){
			$("#alert_warning").trigger("click", 'Description wajib diisi');
			return false
		}
		
		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#update_progres_ticket').attr('action'),
					data: $('#update_progres_ticket').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#update_progres_ticket').trigger("reset");
						$('#adminSupportTable').DataTable().ajax.reload();
						$('#progres_table').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
						$('#detailTicketModal').modal();
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});

	$('#update_comment_ticket').submit(function (event){
		event.preventDefault();
		var comment = $('#comment').val();

		if(!comment){
			$("#alert_warning").trigger("click", 'Status wajib dipilih');
			return false
		}
		
		$('#detailTicketModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				var formData = new FormData($('#update_comment_ticket')[0]);
				$.ajax({
					type: "POST",
					url: $('#update_comment_ticket').attr('action'),
					// data: $('#update_comment_ticket').serialize(),
					data: formData,
					processData: false,
					contentType: false,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#detailTicketModal').modal('hide');
						$('#update_comment_ticket').trigger("reset");
						$('#commentTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
						$('#detailTicketModal').modal();
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#detailTicketModal').modal();
					}
				});
			}
		});
	});

});

function showDetail(ticket_id)
{
	$('#ticket_id').val(ticket_id);
	$('#_ticket_id').val(ticket_id);
	$('#detailTicketModal').modal();
	progresDataTables();

	var dtableProgres = $('#progres_table').dataTable().api();
	$("#progres_table.dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtableProgres.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtableProgres.search("").draw();
			}
			return;
	});
	dtableProgres.draw();
}

function changeTabModal(status)
{
    $('#active_modal_tab').val(status).trigger('change');
}

function progresDataTables()
{
    var ticket_id = $('#ticket_id').val();
	console.log(ticket_id);
  	$('#progres_table').DataTable().destroy();
    $('#progres_table tbody').empty();

    var progres_table = $('#progres_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
			url: '/admin-support/progres',
			data: function(d) {
                return $.extend({}, d, {
					"ticket_id" : ticket_id,
				});
           },
        },
        columns: [
            {data: 'status', name: 'status',searchable:true,orderable:false},
			{data: 'remark', name: 'remark',searchable:true,orderable:false},
			{data: 'user_id', name: 'user_id',searchable:true,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:true,orderable:false},
        ]
    });
}

function showComment()
{
	
		$('#commentTable').DataTable().destroy();
		$('#commentTable tbody').empty();
		var _ticket_id = $('#_ticket_id').val();
		var commentTable = $('#commentTable').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/admin-support/comment',
				data: function(d){
					return $.extend({},d,{
						'_ticket_id' : _ticket_id,
					});
				},
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: 'created_at', name: 'created_at',searchable:true,orderable:true},
				{data: 'user_id', name: 'user_id',searchable:true,orderable:true},
				{data: 'comment', name: 'comment',searchable:true,orderable:true},
				{data: 'action', name: 'action',searchable:true,orderable:true},
			]
		});

}

$('#active_modal_tab').on('change',function()
    {
		var active_tab = $('#active_modal_tab').val();
        if(active_tab == 'progres')
        {
			progresDataTables();

			var dtableProgres = $('#progres_table').dataTable().api();
			$("#progres_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableProgres.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableProgres.search("").draw();
					}
					return;
			});
			dtableProgres.draw();
            
			
        }else if(active_tab == 'comment')
        {
			showComment();

			var dtableComment = $('#commentTable').dataTable().api();
			$("#commentTable.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableComment.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableComment.search("").draw();
					}
					return;
            });
            dtableComment.draw();
            
        }
    });
</script>
@endsection