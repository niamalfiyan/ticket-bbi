$(document).ready( function () {
	$('#detailIssueCategoryTable').DataTable({
		dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
		deferRender:true,
		ajax: {
			type: 'GET',
			url: '/detail-issue/data',
		},
		columns: [
			{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
			{data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'description', name: 'description',searchable:true,orderable:true},
            {data: 'issue_category_id', name: 'issue_category_id',searchable:true,orderable:true},
			{data: 'action', name: 'action',searchable:true,orderable:true},
		]
	});

	var dtable = $('#detailIssueCategoryTable').dataTable().api();
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
	});
	dtable.draw();

	$('#insert_detail_issue').submit(function (event){
		event.preventDefault();
		var name = $('#name').val();
		var issue = $('#select_issue').val();

		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}
		if(!issue){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}
		
		$('#insertDetailIssueModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "POST",
					url: $('#insert_detail_issue').attr('action'),
					data: $('#insert_detail_issue').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#insertDetailIssueModal').modal('hide');
						$('#insert_detail_issue').trigger("reset");
						$('#detailIssueCategoryTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#insertPermissionModal').modal();
					}
				});
			}
		});
	});

	$('#update_issue').submit(function (event){
		event.preventDefault();
		var name = $('#update_name').val();
		
		if(!name){
			$("#alert_warning").trigger("click", 'Nama wajib diisi');
			return false
		}
		
		$('#updateIssueModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if(result){
				$.ajax({
					type: "PUT",
					url: $('#update_issue').attr('action'),
					data: $('#update_issue').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#updateIssueModal').modal('hide');
						$('#update_issue').trigger("reset");
						$('#detailIssueCategoryTable').DataTable().ajax.reload();
						$("#alert_success").trigger("click", 'Data Berhasil disimpan');
					},
					error: function (response) {
						$.unblockUI();
						if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
						$('#updateIssueModal').modal();
					}
				});
			}
		});
	});
	
});

function edit(url)
{
	$.ajax({
		type: "get",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();

		}
	})
	.done(function (response) {
		$('#update_issue').attr('action', response.url_update);
		$('#update_name').val(response.name);
		$('#update_description').val(response.description);
		$('#updateIssueModal').modal();

	});
}

function hapus(url)
{
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		type: "delete",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function (response) {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
		}
	}).done(function ($result) {
		$('#detailIssueCategoryTable').DataTable().ajax.reload();
		$("#alert_success").trigger("click", 'Data Berhasil hapus');
	});
}