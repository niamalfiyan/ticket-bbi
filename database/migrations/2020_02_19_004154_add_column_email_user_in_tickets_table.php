<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEmailUserInTicketsTable extends Migration
{
    
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->string('email_user')->nullable();
            $table->string('email_atasan')->nullable();
        });
    }

    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('email_user');
            $table->dropColumn('email_atasan');
        });
    }
}
