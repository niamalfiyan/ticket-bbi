<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('menus')->truncate();
        DB::table('users')->truncate();
        DB::table('tickets')->truncate();
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TicketSeeder::class);
    }
}
