<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\DetailIssueCategory;
use App\Models\Ticket;
use Carbon\Carbon;

class TicketSeeder extends Seeder
{
    public function run()
    {
        $detail_ticket = DetailIssueCategory::Select('id')->first();

        $tickets = new Ticket();
        $tickets->nik_pegawai = '190600053';
        $tickets->name = 'Niam Alfiyan Ahsan';
        $tickets->title = 'testing ticket';
        $tickets->detail_issue_category_id = $detail_ticket->id;
        $tickets->ticket_category = 'support';
        $tickets->description = 'ini adalah testing ticket';
        $tickets->last_status = 'open';
        $tickets->save();
    }
}
