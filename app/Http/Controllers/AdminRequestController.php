<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Yajra\Datatables\Datatables;

use App\Models\IssueCategory;
use App\Models\DetailIssueCategory;
use App\Models\Ticket;
use App\Models\User;
use App\Models\DetailTicket;
use Illuminate\Http\Request;

use App\Http\Controllers\TicketController as SettingTicket;

class AdminRequestController extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('id', '!=', '123')->pluck('name', 'id')->all();
        return view('admin-request.index',compact('users'));
    }
    
    public function data(Request $request)
    {
        $factory = $request->factory;
        $status  = $request->status;
        if(request()->ajax()) 
        {
        if($status == null or $status == '')
        {
            $data = DB::table('ticket_v')
                    ->where('ticket_category', 'request')
                    ->where('factory', 'like', '%' .$factory. '%')
                    ->orderby('created_at','desc');
        }
        elseif($status == 'waiting-approval')
        {
            $data = DB::table('ticket_v')
                ->where([['ticket_category', 'request'],
                ['date_confirm_host', null]
                ])
                ->where('factory', 'like', '%' .$factory. '%')
                ->orderby('created_at','desc');
        }
        elseif($status == 'waiting-execution')
        {
            $data = DB::table('ticket_v')
                ->where([['ticket_category', 'request'],
                ['date_confirm_host', '!=', null],
                ['last_status', '!=', 'closed']
                ])
                ->where('factory', 'like', '%' .$factory. '%')
                ->orderby('created_at','desc');
        }


        return datatables()->of($data)
        ->editColumn('last_status',function ($data)
            {
                if($data->last_status == 'open') return '<span class="label label-default">Open</span>';
                if($data->last_status == 'closed') return '<span class="label label-success">Closed</span>';
                if($data->last_status == 'reject') return '<span class="label label-danger">Reject</span>';
                else return '<span class="label label-info">'.$data->last_status.'</span>';
            })
        ->editColumn('code',function ($data)
            {
                return '<button type="button" class="label label-info" class="btn btn-link legitRipple"  onclick="showDetail(\''.$data->id.'\')">'.$data->code.'</button>';
            })
        ->editColumn('handled_by',function ($data)
        {
            if($data->handled_by == null)
            {
                return 'Not Set Yet';
            }
            else
            {
                return $data->handled_by;
            }    
        })
        ->addColumn('notifikasi', function($data) 
        {
            if($data->comment == null){            
                return '';
            }
            else{            
                return '<span class="badge badge-danger">'.$data->comment.'</span>';
            }
        })   
        ->rawColumns(['code', 'last_status','notifikasi'])
        ->make(true);
        }
    }

    public function progres(Request $request)
    {
        if(request()->ajax()) 
        {
            $ticket_id = $request->ticket_id;
            $data = DetailTicket::where([      
                    ['ticket_id', $ticket_id],
                    ['comment', null],
                    ])
                    ->orderby('created_at','desc')
                    ->get();
            //dd($ticket_id);
            return datatables()->of($data)
            ->editColumn('user_id',function ($data)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            })
            ->editColumn('status',function ($data)
            {
                if($data->status == 'open') return '<span class="label label-default">Open</span>';
                if($data->status == 'closed') return '<span class="label label-success">Closed</span>';
                if($data->status == 'reject') return '<span class="label label-danger">Reject</span>';
                else return '<span class="label label-info">'.$data->status.'</span>';
            })
            ->rawColumns(['user_id', 'status'])
            ->make(true);
        }
    }

    public function comment(Request $request)
    {
        if(request()->ajax()) 
        {
        $ticket_id = $request->_ticket_id;
        $data = DetailTicket::where([      
            ['ticket_id', $ticket_id],
            ['comment', '!=', null],
            ])->orderby('created_at','desc')
            ->get();
        
        return datatables()->of($data)
        ->editColumn('user_id',function ($data)
        {
            if($data->is_admin == true)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            }
            else
            {
                return $data->Ticket->name;
            }
        })
        ->addColumn('action',function($data){
            $file_name = $data->attachment;
            $link = route('adminRequest.download',$data->id);

            if($data->attachment != null){
                return '<a class="label label-flat border-grey text-grey-600" 
                href="'.$link.'">'.$file_name.'</a>';
            }
            else{
                return '';
            }

        })
        ->rawColumns(['user_id','action'])
        ->make(true);
        }
    }

    public function download($id)
    {
        $file = DetailTicket::where('id', $id)->firstOrFail();
       
        $pathToFile = storage_path('app/public/' . $file->attachment);
        return response()->download($pathToFile);
    }

    public function storeProgres(Request $request)
    {
        $ticket_id = $request->ticket_id;
        $status    = $request->status;

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => $status,
            'remark'    => $request->remark,
            'user_id'   => auth::user()->id,
            'is_admin'  => true,
        ]);
        $detail_ticket_id = $detail_ticket->id;
        $ticket              = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->last_status = $status;
        $ticket->save();

        SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function storeComment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        $comment   = $request->comment;
        
        if ($request->hasFile('r_upload')) 
        {
            $file_name =  $request->r_upload->getClientOriginalName();
            $request->r_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }
        
        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id'  => $ticket_id,
            'comment'    => $comment,
            'user_id'    => auth::user()->id,
            'is_admin'   => true,
            'attachment' => $file_name,
        ]);
        $detail_ticket_id = $detail_ticket->id;
        
        $ticket              = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->save();
        
        SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function updateDueDate(Request $request)
    {
        $ticket_id = $request->__ticket_id;
        $priority   = $request->priority;
        $due_date   = $request->due_date;
        $pic   = $request->pic;
        $remark   = $request->remark;

        //dd($remark);
        //dd($pic);
        $ticket = Ticket::find($ticket_id);
        $ticket->priority = $priority;
        //$ticket->due_date = Carbon::createFromFormat('d/m/Y', $due_date)->format('d/M/Y');
        $ticket->handled_by = $pic;
        $ticket->save();

        $detail_tikcet = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => 'Set Due Date',
            'remark'    => $remark,
            'user_id'   => auth::user()->id,
            'is_admin'  => true,
        ]);


        return response()->json('success', 200);
    }

    // public function approvalGuest(Request $request)
    // {
    //     $ticket_id = $request->ticket_id;
    //     $ticket = Ticket::find($ticket_id);
    //     $ticket->date_confirm_guest = Carbon::now();
    //     $ticket->save();

    //     $detail_ticket = DetailTicket::firstorCreate([
    //         'ticket_id' => $ticket_id,
    //         'status'    => 'Approved By Guest',
    //         'remark'    => null,
    //         'user_id'   => '123',
    //     ]);
    //     $detail_ticket_id = $detail_ticket->id;

    //     SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

    // }

    // public function approvalHost(Request $request)
    // {
    //     $ticket_id = $request->ticket_id;
    //     $ticket = Ticket::find($ticket_id);
    //     $ticket->date_confirm_host = Carbon::now();
    //     $ticket->user_confirm_host = Auth::user()->id;
    //     $ticket->save();

    //     $detail_tikcet = DetailTicket::firstorCreate([
    //         'ticket_id' => $ticket_id,
    //         'status'    => 'Approved By Host',
    //         'remark'    => null,
    //         'user_id'   => '123',
    //     ]);

    //     SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_tikcet->id);

    // }

}
