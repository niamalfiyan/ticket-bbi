<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use StdClass;
use Validator;
use Yajra\Datatables\Datatables;

use App\Models\IssueCategory;
use App\Models\DetailIssueCategory;
use App\Models\Ticket;
use App\Models\User;
use App\Models\DetailTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\TicketController as SettingTicket;

class AdminSupportController extends Controller
{
    public function index(Request $request)
    {
        $comments = DetailTicket::where('comment', '!=', null)
                    ->orderby('created_at','asc')
                    ->get();
        //$comments = array();
        return view('admin-support.index',compact('comments'));
    }
    public function data()
    {
        if(request()->ajax()) 
        {
        $data = DB::table('ticket_v')
                ->where('ticket_category', 'support')
                ->orderby('created_at','desc');
            
        return datatables()->of($data)
        ->editColumn('last_status',function ($data)
            {
                if($data->last_status == 'open') return '<span class="label label-default">Open</span>';
                if($data->last_status == 'closed') return '<span class="label label-success">Closed</span>';
                if($data->last_status == 'reject') return '<span class="label label-danger">Reject</span>';
                else return '<span class="label label-info">'.$data->last_status.'</span>';
            })
        ->editColumn('code',function ($data)
            {
                return '<button type="button" class="label label-info" class="btn btn-link legitRipple"  onclick="showDetail(\''.$data->id.'\')">'.$data->code.'</button>';           
            })
         ->addColumn('notifikasi', function($data) 
            {
                if($data->comment == null){            
                    return '';
                }
                else{            
                    return '<span class="badge badge-danger">'.$data->comment.'</span>';
                }
            })   
        ->rawColumns(['code', 'last_status','notifikasi'])
        ->make(true);
        }
    }

    public function progres(Request $request)
    {
        if(request()->ajax()) 
        {
        $ticket_id = $request->ticket_id;
        $data = DetailTicket::where([      
                ['ticket_id', $ticket_id],
                ['comment', null],
                ])
                ->orderby('created_at','desc')
                ->get();
        //dd($ticket_id);
        return datatables()->of($data)
        ->editColumn('user_id',function ($data)
        {
            return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
        })
        ->editColumn('status',function ($data)
        {
            if($data->status == 'open') return '<span class="label label-default">Open</span>';
            if($data->status == 'closed') return '<span class="label label-success">Closed</span>';
            if($data->status == 'reject') return '<span class="label label-danger">Reject</span>';
            else return '<span class="label label-info">'.$data->status.'</span>';
        })
        ->rawColumns(['user_id', 'status'])
        ->make(true);
        }

    }

    public function comment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        //dd($ticket_id);
        if(request()->ajax()) 
        {
        $data = DetailTicket::where([      
            ['ticket_id', $ticket_id],
            ['comment', '!=', null],
            ])
        ->orderby('created_at','asc')
        ->get();
        return datatables()->of($data)
        ->editColumn('user_id',function ($data)
        {
            if($data->is_admin == true)
            {
                return ($data->is_admin == true)?$data->User->name:$data->Ticket->name;
            }
            else
            {
                return $data->Ticket->name;
            }

        })
        ->addColumn('action',function($data){
            $file_name = $data->attachment;
            $link = route('adminSupport.download',$data->id);

            if($data->attachment != null){
                return '<a class="label label-flat border-grey text-grey-600" 
                href="'.$link.'">'.$file_name.'</a>';
            }
            else{
                return '';
            }

        })
        ->make(true);
        }
    }

    public function download($id)
    {
        $file = DetailTicket::where('id', $id)->firstOrFail();
       
        $pathToFile = storage_path('app/public/' . $file->attachment);
        return response()->download($pathToFile);
    }

    public function storeProgres(Request $request)
    {
        $ticket_id = $request->ticket_id;
        $status    = $request->status;

        $detail_ticket = DetailTicket::firstorCreate([
            'ticket_id' => $ticket_id,
            'status'    => $status,
            'remark'    => $request->remark,
            'user_id'   => auth::user()->id,
            'is_admin'  => true,
        ]);

        $ticket              = Ticket::find($ticket_id);
        $ticket->last_status = $status;
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->save();

        $detail_ticket_id = $detail_ticket->id;

        SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_ticket_id);

        return response()->json('success', 200);
    }

    public function storeComment(Request $request)
    {
        $ticket_id = $request->_ticket_id;
        $comment   = $request->comment;
        
        if ($request->hasFile('s_upload')) 
        {
            $file_name =  $request->s_upload->getClientOriginalName();
            $request->s_upload->storeAs('public', $file_name);
        }
        else{
            $file_name = null;
        }
        
        
        $detail_tikcet = DetailTicket::firstorCreate([
            'ticket_id'  => $ticket_id,
            'comment'    => $comment,
            'is_admin'   => true,
            'user_id'    => auth::user()->id,
            'attachment' => $file_name,
            'is_admin'  => true,
        ]);

        $ticket = Ticket::find($ticket_id);
        if($ticket->firts_reponse_date == null)
        {
            $ticket->firts_reponse_date = Carbon::now();
        }
        $ticket->save();

        //SettingTicket::sendEmailUpdateTicket($ticket_id, $detail_tikcet->id);

        return response()->json('success', 200);
    }
}
