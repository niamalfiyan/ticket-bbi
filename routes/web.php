<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/ticket');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

Route::prefix('/permission')->middleware(['permission:menu-permission'])->group(function(){
    Route::get('', 'PermissionController@index')->name('permission.index');
    Route::get('data', 'PermissionController@data')->name('permission.data');
    Route::post('store', 'PermissionController@store')->name('permission.store');
    Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
    Route::put('update/{id}', 'PermissionController@update')->name('permission.update');
    Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
});

Route::prefix('/role')->middleware(['permission:menu-role'])->group(function(){
    Route::get('', 'RoleController@index')->name('role.index');
    Route::get('data', 'RoleController@data')->name('role.data');
    Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
    Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
    Route::put('update/{id}', 'RoleController@update')->name('role.update');
    Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
});

Route::prefix('/user')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'UserController@index')->name('user.index');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
    Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
    Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::delete('delete/{id}', 'UserController@destroy')->name('user.destroy');
});

Route::prefix('/issue')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'IssueCategoryController@index')->name('issue.index');
    Route::get('data', 'IssueCategoryController@data')->name('issue.data');
    Route::get('edit/{id}', 'IssueCategoryController@edit')->name('issue.edit');
    Route::post('store', 'IssueCategoryController@store')->name('issue.store');
    Route::put('update/{id}', 'IssueCategoryController@update')->name('issue.update');
    Route::delete('delete/{id}', 'IssueCategoryController@destroy')->name('issue.destroy');
});

Route::prefix('/detail-issue')->middleware(['permission:menu-user'])->group(function(){
    Route::get('', 'DetailIssueCategoryController@index')->name('detailIssue.index');
    Route::get('data', 'DetailIssueCategoryController@data')->name('detailIssue.data');
    Route::get('edit/{id}', 'DetailIssueCategoryController@edit')->name('detailIssue.edit');
    Route::post('store', 'DetailIssueCategoryController@store')->name('detailIssue.store');
    Route::put('update/{id}', 'DetailIssueCategoryController@update')->name('detailIssue.update');
    Route::delete('delete/{id}', 'DetailIssueCategoryController@destroy')->name('detailIssue.destroy');
});

Route::prefix('/ticket')->group(function(){
    
    //web //fungsi //panggil
    Route::get('', 'TicketController@index')->name('ticket.index');
    Route::post('logon', 'TicketController@logon')->name('ticket.logon');
    Route::get('home/{id}', 'TicketController@home')->name('ticket.home');

    Route::get('list-issue', 'TicketController@listIssue')->name('ticket.list_issue');
    Route::get('list-detail-issue', 'TicketController@listDetailIssue')->name('ticket.list_detail_issue');
    
    Route::post('store-support', 'TicketController@storeSupport')->name('ticket.store_support');
    Route::post('store-request', 'TicketController@storeRequest')->name('ticket.store_request');

    Route::get('history', 'TicketController@dataHistory')->name('ticket.dataHistory');
    Route::get('progress', 'TicketController@dataProgress')->name('ticket.dataProgress');
    Route::get('comment', 'TicketController@dataComment')->name('ticket.dataComment');
    Route::post('add-comment', 'TicketController@addComment')->name('ticket.addComment');
    Route::get('download/{id}', 'TicketController@download')->name('ticket.download');
    Route::get('close/{id}', 'TicketController@close')->name('ticket.close');
    Route::post('close-update/{id}', 'TicketController@close_update')->name('ticket.close_update');
    Route::get('cancel/{id}', 'TicketController@cancel')->name('ticket.cancel');
    Route::post('cancel-update/{id}', 'TicketController@cancel_update')->name('ticket.cancel_update');

    Route::get('email', 'TicketController@email')->name('ticket.email');
});

Route::prefix('/admin-support')->group(function(){
    Route::get('', 'AdminSupportController@index')->name('adminSupport.index');
    Route::get('data', 'AdminSupportController@data')->name('adminSupport.data');
    Route::post('store-progres', 'AdminSupportController@storeProgres')->name('adminSupport.storeProgres');
    Route::post('store-comment', 'AdminSupportController@storeComment')->name('adminSupport.storeComment');
    Route::get('progres', 'AdminSupportController@progres')->name('adminSupport.progres');
    Route::get('comment', 'AdminSupportController@comment')->name('adminSupport.comment');
    Route::get('download/{id}', 'AdminSupportController@download')->name('adminSupport.download');
});

Route::prefix('/admin-request')->group(function(){
    Route::get('', 'AdminRequestController@index')->name('adminRequest.index');
    Route::get('data', 'AdminRequestController@data')->name('adminRequest.data');
    Route::post('store-progres', 'AdminRequestController@storeProgres')->name('adminRequest.storeProgres');
    Route::post('update-duedate', 'AdminRequestController@UpdateDueDate')->name('adminRequest.updateDueDate');
    Route::post('store-comment', 'AdminRequestController@storeComment')->name('adminRequest.storeComment');
    Route::get('progres', 'AdminRequestController@progres')->name('adminRequest.progres');
    Route::get('comment', 'AdminRequestController@comment')->name('adminRequest.comment');
    Route::get('download/{id}', 'AdminRequestController@download')->name('adminRequest.download');
});

Route::prefix('/approval')->group(function(){
    Route::post('guest/{id}', 'AdminRequestController@approvalGuest')->name('adminRequest.approvalGuest');
    Route::post('host/{id}', 'AdminRequestController@approvalHost')->name('adminRequest.approvalHost');
});